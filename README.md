# Bundle name in tabs

## Purpose
To simplify user navigation, this module supplements the tabs with the name of the bundle or vocabulary.

## Configuration
The module does not require any specific configuration.

## Translation

Use _user interface translation_ at `/admin/config/regional/translate` and search for `View @label`, `Edit @label` and `Delete @label`.
